//
//  DarboDjoSpeed.swift
//  Darbo
//
//  Created by Boo on 17/02/17.
//  Copyright © 2017 Doday corp. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DarboDjoSpeed: UIView {
    
    var origin: CGPoint!
    var delegate: DarboSpeedDelegate?
    
    var state = DjoState(rawValue: 0)
    
    private var urlParameters = [String:String]()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch occur")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first?.location(in: superview) ?? origin
        if touch!.y > 0 && touch!.y < 240 {
            self.center.y = (touches.first?.location(in: superview).y) ?? origin.y
            if touch!.y > 120 {
                switch touch!.y {
                case 140..<160:
                    self.setSpeed(speed: .speed1, currentState: self.state!, toState: .s1, direction: true)
                case 160..<180:
                    self.setSpeed(speed: .speed2, currentState: self.state!, toState: .s2, direction: true)
                case 180..<200:
                    self.setSpeed(speed: .speed3, currentState: self.state!, toState: .s3, direction: true)
                case 200..<220:
                    self.setSpeed(speed: .speed4, currentState: self.state!, toState: .s4, direction: true)
                case 220..<250:
                    self.setSpeed(speed: .speed5, currentState: self.state!, toState: .s5, direction: true)
                default:
                    break
                }
            } else if touch!.y < 120 {
                switch touch!.y {
                case 100..<120:
                    self.setSpeed(speed: .speed1, currentState: self.state!, toState: .b5, direction: false)
                case 80..<100:
                    self.setSpeed(speed: .speed2, currentState: self.state!, toState: .b4, direction: false)
                case 60..<80:
                    self.setSpeed(speed: .speed3, currentState: self.state!, toState: .b3, direction: false)
                case 40..<60:
                    self.setSpeed(speed: .speed4, currentState: self.state!, toState: .b2, direction: false)
                case 20..<40:
                    self.setSpeed(speed: .speed5, currentState: self.state!, toState: .b1, direction: false)
                default:
                    break
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.delegate?.speedDjoState(ldir: 1, rdir: 1, speed: 0)
        UIView.animate(withDuration: 0.4, animations: {
            self.center = self.origin
        })
    }
    
    func setSpeed(speed: MotorSpeed, currentState: DjoState, toState: DjoState, direction: Bool) {
        if self.state != toState {
            print("bla")
            if direction {
                self.delegate?.speedDjoState(ldir: 0, rdir: 0, speed: speed.rawValue)
            } else {
                self.delegate?.speedDjoState(ldir: 1, rdir: 1, speed: speed.rawValue)
            }
        }
        self.state = toState
    }
    
}

// Speed in Herz(0-99) change if use arduino(0-255)
enum MotorSpeed: Int {
    case speed1 = 20
    case speed2 = 40
    case speed3 = 60
    case speed4 = 80
    case speed5 = 99
}


// don't change only for checking
enum DjoState: Int {
    case b5 = 100
    case b4 = 80
    case b3 = 60
    case b2 = 40
    case b1 = 20
    case zero = 0
    case s1 = 140
    case s2 = 160
    case s3 = 180
    case s4 = 200
    case s5 = 220
}

struct Parameters {
    var ldirection = 0
    var rdirection = 0
    var lspeed = 0
    var rspeed = 0
    var speed = 0
}
