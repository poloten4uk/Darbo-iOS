//
//  DarboController.swift
//  Darbo
//
//  Created by Boo on 17/02/17.
//  Copyright © 2017 Doday corp. All rights reserved.
//

import UIKit
import Alamofire

protocol DarboSpeedDelegate {
    func speedDjoState(ldir: Int, rdir: Int, speed: Int)
}

protocol DarboSideDelegate {
    func setSideState(rmotor: Double, lmotor: Double)
}

class DarboController: UIViewController, DarboSideDelegate, DarboSpeedDelegate {
    
    @IBOutlet var djoSpeed: DarboDjoSpeed!
    @IBOutlet var djoSide: DarboDjoSide!
    
    var ip = ""
    
    private var urlParameters = [String:String]()
    
    private var sideState = (0.0, 0.0) {
        didSet {
            print(sideState)
        }
    }
    
    private var parameters = Parameters()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        djoSpeed.layer.cornerRadius = 30
        djoSide.layer.cornerRadius = 30
        djoSpeed.origin = djoSpeed.center
        djoSide.origin = djoSide.center
        djoSpeed.delegate = self
        djoSide.delegate = self
    }
    
    func speedDjoState(ldir: Int, rdir: Int, speed: Int) {
        parameters.ldirection = ldir
        parameters.rdirection = rdir
        parameters.speed = speed
        if self.sideState != (0.0, 0.0) {
            parameters.lspeed = Int(Double(speed) * self.sideState.0)
            parameters.rspeed = Int(Double(speed) * self.sideState.1)
        } else {
            parameters.lspeed = speed
            parameters.rspeed = speed
        }
        makeRequest()
    }
    
    func setSideState(rmotor: Double, lmotor: Double) {
        self.sideState = (rmotor, lmotor)
        self.speedDjoState(ldir: parameters.ldirection, rdir: parameters.rdirection, speed: parameters.speed)
    }
    
    private func makeRequest() {
        urlParameters["ldirection"] = String(parameters.ldirection)
        urlParameters["lspeed"] = String(parameters.lspeed)
        urlParameters["rdirection"] = String(parameters.rdirection)
        urlParameters["rspeed"] = String(parameters.rspeed)
        _ = Alamofire.request("http://\(ip):5000/", parameters: urlParameters)
        print(ip)
    }
    
}

