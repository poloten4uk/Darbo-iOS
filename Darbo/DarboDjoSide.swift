//
//  DarboDjoSide.swift
//  Darbo
//
//  Created by Boo on 17/02/17.
//  Copyright © 2017 Doday corp. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DarboDjoSide: UIView {
    
    var origin: CGPoint!
    var delegate: DarboSideDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch occur")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first?.location(in: superview) ?? origin
        if touch!.x > 0 && touch!.x < 240 {
            self.center.x = (touches.first?.location(in: superview).x) ?? origin.x
            if touch!.x > 120 {
                switch touch!.x {
                case 140:
                    self.left(touch: Double(touch!.x))
                case 160:
                    self.left(touch: Double(touch!.x))
                case 180:
                    self.left(touch: Double(touch!.x))
                case 200:
                    self.left(touch: Double(touch!.x))
                case 220:
                    self.left(touch: Double(touch!.x))
                default:
                    break
                }
            } else if touch!.x < 120 {
                switch touch!.x {
                case 100:
                    self.right(touch: Double(touch!.x))
                case 80:
                    self.right(touch: Double(touch!.x))
                case 60:
                    self.right(touch: Double(touch!.x))
                case 40:
                    self.right(touch: Double(touch!.x))
                case 20:
                    self.right(touch: Double(touch!.x))
                default:
                    break
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.setSideState(rmotor: 1.0, lmotor: 1.0)
        UIView.animate(withDuration: 0.4, animations: {
            self.center = self.origin
        })
    }
    
    func left(touch: Double) {
        let rcoof:Double = (120 - touch) / 120
        let lcoof:Double = 1 - rcoof
        delegate?.setSideState(rmotor: 1.0, lmotor: lcoof)
    }
    
    func right(touch: Double) {
        let lcoof:Double = (touch - 120) / 120
        let rcoof:Double = 1 - lcoof
        delegate?.setSideState(rmotor: rcoof, lmotor: 1.0)
    }
    
}
