//
//  RobotIPViewController.swift
//  Darbo
//
//  Created by Boo on 24/02/17.
//  Copyright © 2017 Doday corp. All rights reserved.
//

import Foundation
import UIKit

class RobotIPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var ipfield: UITextField!
    
    override func viewDidLoad() {
        print(UserDefaults.standard.value(forKey: "ip") ?? "no ip's")
    }
    
    @IBAction func setIP() {
        performSegue(withIdentifier: "ShowControls", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowControls" {
            let vc = segue.destination as! DarboController
            guard (ipfield.text != nil) else { return }
            vc.ip = ipfield.text!
            var ips = UserDefaults.standard.value(forKey: "ip") as? [String:String]
            guard ips != nil else {
                UserDefaults.standard.setValue([vc.ip: vc.ip], forKey: "ip")
                return
            }
            
            if ips![vc.ip] == nil {
                ips![vc.ip] = vc.ip
                UserDefaults.standard.setValue(ips!, forKey: "ip")
                return
            }
        }
    }
    
    
}
